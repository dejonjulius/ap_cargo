import 'package:flutter/material.dart';
import 'package:ap_cargo/screens/splash-screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Montserrat',
        primaryColor: Colors.black,
        accentColor: Colors.white,
      ),
      home: SplashScreen(),
    );
  }
}
