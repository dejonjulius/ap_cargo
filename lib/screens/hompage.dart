import 'package:flutter/material.dart';
import 'package:ap_cargo/components/bottom-navbar.dart';
import 'package:ap_cargo/components/homepage-component.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[HomePageComponent()],
        ),
      ),
      bottomNavigationBar: BottomNavBar(),
    );
  }
}
