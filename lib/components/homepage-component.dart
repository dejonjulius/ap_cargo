import 'package:flutter/material.dart';
import 'package:ap_cargo/helpers/custom-shape-clipper.dart';

class HomePageComponent extends StatefulWidget {
  @override
  _HomePageComponentState createState() => _HomePageComponentState();
}

class _HomePageComponentState extends State<HomePageComponent> {
  @override
  void initState() {
    super.initState();
  }

  final _primaryColor = Color(0xFFFCAE18);
  final _secondaryColor = Color(0xFF41463F);

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
              height: 200.0,
              decoration: BoxDecoration(
                color: _primaryColor,
              ),
              child: Column(children: <Widget>[
                SizedBox(
                  height: 24.0,
                ),
                Image.asset(
                  'assets/images/ap-cargo-logo.jpg',
                  width: 130.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
                SizedBox(
                  height: 14.0,
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 24.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                          color: _secondaryColor,
                          child: Icon(
                            Icons.local_shipping,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          onPressed: () {},
                        ),
                        RaisedButton(
                          color: _secondaryColor,
                          child: Icon(
                            Icons.motorcycle,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          onPressed: () {},
                        ),
                        RaisedButton(
                          color: _secondaryColor,
                          child: Icon(
                            Icons.create,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          onPressed: () {},
                        ),
                      ],
                    )),
              ])))
    ]);
  }
}
